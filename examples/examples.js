const inquirer = require('inquirer');
inquirer.registerPrompt('listOrder', require('../index'));

inquirer.prompt([{
  type: 'listOrder',
  message: 'Change list order',
  name: 'list',
  choices: [{
    name: "France",
    value: "fr"
  }, {
    name: "England",
    value: "en"
  }, {
    name: "Germany",
    value: "ge"
  }, {
    name: "Spain",
    value: "sp"
  }]
}]).then(function(answers) {
  console.log('Change list order', answers.list); // eslint-disable-line no-console
});
