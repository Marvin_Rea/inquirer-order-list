// require("inquirer");
const Base = require("inquirer/lib/prompts/base");
const observe = require("inquirer/lib/utils/events");
const Paginator = require("inquirer/lib/utils/paginator");
const chalk = require("chalk");
const figures = require("figures");
const cliCursor = require("cli-cursor");
var { filter, share, takeUntil, map } = require('rxjs/operators');

/**
 * Function for rendering list choices
 * @param  {Array} choices array with choices
 * @param  {Number} pointer Position of the pointer
 * @param  {Boolean} isSelected is item selected
 * @return {String}         Rendered content
 */
function listRender(choices, pointer, isSelected) {
  let output = "";

  choices.forEach(function (choice, i) {
    if (choice.type === "separator") {
      output += "  " + i.toString() + " - " + choice.name + "\n";
      return;
    }

    const isPointed = i === pointer;
    let line = isPointed ? chalk.cyan(figures.pointer) + " " : "  "
    line += isPointed ? chalk.cyan(i.toString() + " - ") : i.toString() + " - ";
    if (isPointed) {
      if (isSelected) {
        line += chalk.green(choice.name);
      } else {
        line += chalk.cyan(choice.name);
      }
    } else line += choice.name;
    output += line + " \n";
  });

  return output.replace(/\n$/, "");
}

/**
 * Constructor
 * @return {this} Prompt object
 */
class Prompt extends Base {
  constructor(...args) {
    super(...args);
    (this.opt = {
      ...args[0],
      suffix: "",
      prefix: ""
    }),
      (this.line = 0);
    this.selected = null;
    this.paginator = new Paginator();
    this.firstRender = true;
    this.choices = this.opt.choices.map(e =>
      typeof e === "string" ? { name: e, value: e } : e
    );

    const events = observe(args[1]);
    events.normalizedDownKey
      .pipe(takeUntil(events.line))
      .forEach(this.onDownKey.bind(this));
    events.normalizedUpKey
      .pipe(takeUntil(events.line))
      .forEach(this.onUpKey.bind(this));
    events.spaceKey
      .pipe(takeUntil(events.line))
      .forEach(this.onSpaceKey.bind(this));
    events.line.forEach(this.onSubmit.bind(this));
    return this;
  }

  /**
   * handle pressed space key
   */
  onSpaceKey() {
    if (this.selected === null) {
      this.selected = this.choices[this.line];
      this.choices.splice(this.line, 1);
    } else {
      this.choices.splice(this.line, 0, this.selected);
      this.selected = null;
    }
    this.render();
  }

  /**
   * handle pressed down key
   */
  onDownKey() {
    const len = this.choices.length;
    this.line = this.line < len ? this.line + 1 : 0;
    this.render();
  }

  /**
   * handle pressed up key
   */
  onUpKey() {
    const len = this.choices.length;
    this.line = this.line > 0 ? this.line - 1 : len;
    this.render();
  }

  /**
   * handle pressed enter key
   */
  onSubmit() {
    if (this.selected !== null) {
      this.choices.splice(this.line, 0, this.selected);
      this.selected = null;
    }
    this.status = "answered";
    // Rerender prompt
    this.render();

    this.screen.done();
    cliCursor.show();
    this.done(this.choices.map(e => e.value));
  }

  /**
   * Start the Inquiry session
   * @param  {Function} cb Callback when prompt is done
   * @return {this} Prompt object
   */
  _run(cb) {
    this.done = cb;
    cliCursor.hide();
    this.render();
    return this;
  }

  /**
   * render prompt to screen
   */
  render() {
    let message = this.getQuestion();

    if (this.firstRender) {
      message += chalk.dim("(Use arrow keys and space to select/deselect)");
    }

    let choices =
      this.selected !== null
        ? [
          ...this.choices.slice(0, this.line),
          this.selected,
          ...this.choices.slice(this.line)
        ]
        : this.choices;

    const choicesStr = listRender(choices, this.line, this.selected !== null);
    message +=
      "\n" + this.paginator.paginate(choicesStr, this.line, this.opt.pageSize);

    this.firstRender = false;
    this.screen.render(message);
  }
}

module.exports = Prompt;
